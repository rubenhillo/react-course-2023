

export default function Tab({ isActive, label, onNewTabSelected }) {
    return (
        <>
            {
                isActive
                    ? <a href="#" onClick={() => onNewTabSelected()} class="inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900">{label}</a>
                    : <a href="#" onClick={() => onNewTabSelected()} class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">{label}</a>
            }
        </>
    )
}