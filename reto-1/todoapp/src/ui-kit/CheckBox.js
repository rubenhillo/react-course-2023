

export default function CheckBox({ id, label }) {
    return (
        <>
            <input
                id={id}
                name={id}
                type="checkbox"
                class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
            />
            <label for="remember-me" class="ml-3 block text-sm leading-6 text-gray-900">
                {label}
            </label>
        </>
    )
}