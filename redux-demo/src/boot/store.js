import { createStore, combineReducers } from 'redux'
import userReducer from '../reducers/users'
import taskReducer from '../reducers/tasks'

const reducers = combineReducers({
    users: userReducer,
    tasks: taskReducer
})

export const store = createStore(reducers)