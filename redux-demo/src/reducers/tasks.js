import { ADD_NEW_TASK } from "../types/tasks"

const initialState = {

    tasks: [],

    task: {
        name: '',
        description: '',
        dueDate: new Date(),
        status: 'pending',
        author: null
    }

}

function taskReducer(state = initialState, action) {
    switch (action.type) {

        case ADD_NEW_TASK:
            const newTasks = [...state.tasks, action.payload]
            return { ...state, tasks: newTasks }

        default:
            return state
    }
}

export default taskReducer