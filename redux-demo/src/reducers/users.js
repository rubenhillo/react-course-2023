import {
	INCREMENT_COUNTER,
	DECREASE_COUNTER,
	UPDATE_USER_NAME,
	UPDATE_USER_LASTNAME,
} from "../types/users";

const initialState = {
	name: "Victor",
	lastName: "Lopez",
	age: 29,
	email: "lopez.victor94@gmail.com",
	counter: 0,
	min: 0,
	max: 20,
};

export default function userReducer(state = initialState, action) {
	switch (action.type) {
		case UPDATE_USER_NAME:
			return { ...state, name: action.payload };

		case UPDATE_USER_LASTNAME:
			return { ...state, lastName: action.payload };

		case INCREMENT_COUNTER:
			if (state.counter === state.max) {
				return state;
			}
			return { ...state, counter: state.counter + 1 };

		case DECREASE_COUNTER:
			if (state.counter === state.min) {
				return state;
			}
			return { ...state, counter: state.counter - 1 };

		default:
			return state;
	}
}
