import {
	DECREASE_COUNTER,
	INCREMENT_COUNTER,
	UPDATE_USER_LASTNAME,
	UPDATE_USER_NAME,
} from "../types/users";

export const incrementCounter = () => ({
	type: INCREMENT_COUNTER,
});

export const decrementCounter = () => ({
	type: DECREASE_COUNTER,
});

export const updateUserData = (property, val) => {
	switch (property) {
		case "name":
			return {
				type: UPDATE_USER_NAME,
				payload: val,
			};
		case "lastname":
			return {
				type: UPDATE_USER_LASTNAME,
				payload: val,
			};
		default:
			return {
				type: UPDATE_USER_NAME,
			};
	}
};
