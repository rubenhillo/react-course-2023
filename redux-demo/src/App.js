import logo from "./logo.svg";
import "./App.css";

import { useSelector, useDispatch } from "react-redux";
import {
	incrementCounter,
	decrementCounter,
	updateUserData,
} from "./actions/users";

function App() {
	const users = useSelector((state) => state.users);
	const dispatch = useDispatch();

	return (
		<div className="App">
			<h1>Nombre del usuario: {users.name} {users.lastName}</h1>
			<h3>Contador: {users.counter}</h3>
			<input
        placeholder="Nombre"
				onChange={(e) => dispatch(updateUserData("name", e.target.value))}
			/>
      <input
        placeholder="Apellido"
				onChange={(e) => dispatch(updateUserData("lastname", e.target.value))}
			/>
			<button onClick={() => dispatch(incrementCounter())}>
				Incrementar contador
			</button>
			<button onClick={() => dispatch(decrementCounter())}>
				Decrementar contador
			</button>
		</div>
	);
}

export default App;
