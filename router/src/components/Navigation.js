import { Link } from "react-router-dom"

export default function Navigation() {
    const id = 18
    const name = 'Vic'

    return (
        <>
            <h2>Navegacion</h2>
            <Link to={'/fede'}>Fede</Link> <br />
            <Link to={'/profe'}>Profe</Link> <br />
            <Link to={`/parameter/${id}/${name}`}>Ruta con parametro</Link>
        </>
    )
}