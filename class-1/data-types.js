// String
let myString = '1'

// Int
let myInt = 1

// Arreglo
let myArray = [1,2,3,4]

// Boolean
let myBool = true

// JSON
let myObject = {}

const MY_CONST = 'MY CONSTANT'

/*
if (myString === myInt) {
    console.log('equals')
} else if (myInt !== myInt) {
    console.log('different')
} else {
    console.log('nothing')
}


for (let i = 0; i < myArray.length; i++) {
    console.log(myArray[i])
}

*/


