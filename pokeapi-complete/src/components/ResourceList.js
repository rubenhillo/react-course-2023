import ResourceItem from "./ResourceItem"

export default function ResourceList({ resources, sprite_key }) {
    return (
        resources.map(resource => {
            return <ResourceItem resource={resource} sprite_key={sprite_key} />
        })
    )
}