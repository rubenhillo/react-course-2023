import { useState } from "react";
import TextInput from "../ui-kit/TextInput";


export default function NewList({ onNewTaskAdded }) {

    const [name, setName] = useState('')
    const [description, setDesciption] = useState('')
    const [color, setColor] = useState('')
    const [dueDate, setDueDate] = useState('')

    const addNewTask = (e) => {
        e.preventDefault()
        onNewTaskAdded({ name, description, color, dueDate })
        setName('')
        setDesciption('')
        setColor('')
        setDueDate('')
    }

    return (
        <>
            <form
                class="space-y-6"
                method="POST"
                onSubmit={addNewTask}
            >
                <div>
                    <TextInput
                        label={'Nombre de la tarea'}
                        id={'name'}
                        type={'text'}
                        change={val => setName(val)}
                        value={name}
                    />
                </div>
                <div>
                    <TextInput
                        label={'Descripción'}
                        id={'description'}
                        type={'text'}
                        change={val => setDesciption(val)}
                        value={description}
                    />
                </div>
                <div>
                    <TextInput
                        label={'Color de la tarea'}
                        id={'color'}
                        type={'text'}
                        change={val => setColor(val)}
                        value={color}
                    />
                </div>
                <div>
                    <TextInput
                        label={'Fecha de entrega'}
                        id={'dueDate'}
                        type={'text'}
                        change={val => setDueDate(val)}
                        value={dueDate}
                    />
                </div>
                <button type="submit" class="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                    Agregar tarea
                </button>
            </form>
        </>
    )
}