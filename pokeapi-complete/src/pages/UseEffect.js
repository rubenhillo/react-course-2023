import { useEffect, useState } from 'react'


export default function UseEffect() {

    const [counter, setCounter] = useState(0)
    const [triples, setTriples] = useState(0)
    const [changes, setChanges] = useState(-1)

    const [mouseHasEnter, setMouseHasEnter] = useState(0)

    // Unica y exclusivamente cuando el componente carga por primera vez
    useEffect(() => {

    }, [])

    // Cada que el componente se renderiza, ya sea por un cambio o por primera vez
    useEffect(() => {

    })

    // Cuando el componente se monta por primera vez y cuando la variable counter cambie
    useEffect(() => {
        setTriples(counter * 3)
        setChanges(changes + 1)
    }, [counter])

    useEffect(() => {
        setChanges(changes + 1)
    }, [triples])

    // Counter = 1          Triples = 3         Changes = 2
    // Counter = 2          Triples = 6         Changes = 4
    // Counter = 3          Triples = 9         Changes = 6

    return (
        <>
            <h1>Counter inicial {counter}</h1>
            <h1>Triples {triples}</h1>
            <h1>Cambios {changes}</h1>



            <button
                className='p-2 bg-blue-500 text-white rounded hover:bg-red-500'
                onClick={() => setCounter(counter + 1)}
            >
                Increment
            </button>
        </>
    )
}