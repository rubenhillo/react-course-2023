import { useEffect, useState } from "react"
import { getDataFromResource, getNewData, getResource } from "../helpers/apiCalls"
import ResourceList from "../components/ResourceList"
import Filters from "./Filters"

export default function Catalog({ customFilters, resource, sprite_key }) {

    const [list, setList] = useState([])
    const [nextPage, setNextPage] = useState()
    const [prevPage, setPrevPage] = useState()

    const [filters, setFilters] = useState()

    const setResourceData = data => {
        setNextPage(data.next)
        setPrevPage(data.previous)
        setList(data.results)
    }

    const setFilterInit = async (key, resource, title) => {
        const res = await getDataFromResource(resource)
        setFilters((filters) => ({
            ...filters,
            [key]: {
                name: title,
                selectedFilters: [],
                data: res.data.results
            }
        }))
    }

    const init = async () => {


        customFilters.forEach(filter => {
            setFilters(filters => ({
                ...filters,
                [filter.key]: {}
            }))
            setFilterInit(filter.key, filter.resource, filter.title)
        })

        let res = await getDataFromResource(resource)
        setResourceData(res.data)

    }

    const getDataFromFilterObject = async (filter) => {
        let res = await getResource(filter.url)
        return res.data[resource + 's'].map(entity => entity)
    }

    const updateFiltersInStateWithData = async (allFilters, newFilters, key) => {
        let allResponses = []
        Object.values(allFilters).forEach(entity => (entity.forEach(fil => allResponses.push(getDataFromFilterObject(fil, key)))))
        allResponses = await Promise.all(allResponses)

        console.log('all res', allResponses)
        setFilters({
            ...filters,
            [key]: {
                ...filters[key],
                selectedFilters: newFilters,
            }
        })

        let allData = []
        allResponses.forEach(entity => entity.forEach(element => allData.push(element)))

        setList(allData)
    }

    const deleteFilter = async (filter, key) => {
        // Tomar los filtros del estado y los agrega a una variable local
        let newFilters = [...filters[key].selectedFilters]
        // Genera la estructura de filtros original en una variable local
        let allFilters = {
            habilities: [...filters['habilities']['selectedFilters']],
            types: [...filters['types']['selectedFilters']],
        }

        // Filtrar para que el filtro que se quiere eliminar salga del arreglo
        newFilters = newFilters.filter(objectFilter => objectFilter.name !== filter.name)
        // Esto es lo que agrega a la estructura el filtro nuevo
        allFilters[key] = newFilters

        // Función encargada de obtener la información nueva y actualizar el estado
        updateFiltersInStateWithData(allFilters, newFilters, key)
    }

    const updateFilters = async (filter, key) => {
        // Tomar los filtros del estado y los agrega a una variable local
        const newFilters = [...filters[key].selectedFilters]
        // Agrega el nuevo filtro a nuestra variable local
        newFilters.push(filter)

        // Genera la estructura de filtros original en una variable local
        let allFilters = {}
        customFilters.forEach(fil => {
            const filterKey = fil.key
            allFilters[filterKey] = [...filters[filterKey]['selectedFilters']]
        })

        // Esto es lo que agrega a la estructura el filtro nuevo
        allFilters[key] = newFilters

        // Función encargada de obtener la información nueva y actualizar el estado
        updateFiltersInStateWithData(allFilters, newFilters, key)
    }

    const getNewPage = async url => {
        const res = await getNewData(url)
        setResourceData(res.data)
    }

    useEffect(() => {
        init()
    }, [])

    return (
        <>
            <div className="mx-auto max-w-7xl p-4 sm:px-6 lg:px-8" >

                <Filters
                    filters={filters}
                    onNewFilterSelected={({ filter, key }) => updateFilters(filter, key)}
                    onFilterDelete={({ filter, key }) => deleteFilter(filter, key)}
                >

                    <div className="bg-white">

                        {prevPage && <button onClick={() => getNewPage(prevPage)} className='p-2 bg-indigo-500 rounded text-white mr-3'>Página anterior</button>}
                        {nextPage && <button onClick={() => getNewPage(nextPage)} className='p-2 bg-indigo-500 rounded text-white mr-3'>Página siguiente</button>}

                        <div class="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                            {
                                list.length > 0 && <ResourceList resources={list} sprite_key={sprite_key} />
                            }
                        </div>
                    </div>

                </Filters>

            </div >
        </>
    )
}