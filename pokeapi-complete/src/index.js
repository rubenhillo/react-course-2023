import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import reportWebVitals from './reportWebVitals'
import NavBar from "./layout/Navbar"
import Footer from "./layout/Footer"
import Home from "./pages/Home"
import Register from "./pages/Register"
import TodoApp from './components/TodoApp'
import Items from './pages/Items'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <NavBar />

            <Routes>
                <Route path={'/'} element={<Home />} />
                <Route path={'/registro'} element={<Register />} />
                <Route path={'/items'} element={<Items />} />
                <Route path={'/todoapp'} element={<TodoApp />} />
            </Routes>

            <Footer />
        </BrowserRouter>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
