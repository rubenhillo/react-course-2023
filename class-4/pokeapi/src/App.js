import './index.css'
import Home from './pages/Home'
import Filters from './pages/Filters'
import UseEffect from './pages/UseEffect'

export default function App() {

    return (
        <>
            <div class="mx-auto max-w-7xl sm:px-6 lg:px-8" >

                <UseEffect />

            </div >
        </>
    )

}