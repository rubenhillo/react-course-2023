import React, { useState, useEffect } from "react"
import { getAllPokemons } from "../helpers/apiCalls"
import Footer from "../layout/Footer"
import NavBar from "../layout/Navbar"
import PokemonList from "../components/PokemonList"
import SignUpForm from "../forms/SignUpForm"

export default function Home() {

    const [pokemons, setPokemons] = useState([])
    const [selectedTab, setSelectedTab] = useState(3)


    const init = async () => {
        const res = await getAllPokemons()
        setPokemons(res.data.results)
    }

    useEffect(() => {
        init()
    })

    return (
        <>
            <NavBar onNewTabSelected={tabSelected => setSelectedTab(tabSelected)} />

            <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">

                {
                    selectedTab === 0 &&
                    <div className="bg-white">
                        <div class="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                            {pokemons.length > 0 && <PokemonList pokemons={pokemons} />}
                        </div>
                    </div>
                }

                {
                    selectedTab === 1 &&
                    <div className="h-full bg-gray-50">
                        <div class="h-full">
                            <SignUpForm />
                        </div>
                    </div>
                }

                {
                    selectedTab === 2 &&
                    <div className="bg-white">
                        <div class="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                            <h1>Espíritus</h1>
                        </div>
                    </div>
                }



            </div>

            <Footer />
        </>
    )
}