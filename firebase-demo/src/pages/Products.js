import { collection, getDocs } from "firebase/firestore"
import { db } from "../firebase"
import { useEffect } from "react"

export default function Products() {


    const init = async () => {
        const snapshot = await getDocs(collection(db, 'products'))

        snapshot.forEach((doc) => {
            console.log('product', doc.id, doc.data())
        })
    }

    useEffect(() => {
        init()
    }, [])

    return (
        <>

        </>
    )
}