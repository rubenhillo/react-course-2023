import productsReducer from '../reducers/productsReducer'
import { applyMiddleware, createStore, combineReducers } from 'redux'
import thunk from 'redux-thunk'

const reducers = combineReducers({
    products: productsReducer
})

export const store = createStore(reducers, applyMiddleware(thunk))
