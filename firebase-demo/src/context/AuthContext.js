import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'firebase/auth'
import { auth } from '../firebase'
import { createContext, useContext, useEffect } from 'react'
import { useState } from 'react'

export const authContext = createContext();

export const useAuth = () => {
    const context = useContext(authContext)
    if (!context) {
        throw new Error('El contexto no esta definido')
    }
    return context
}

export function AuthProvider({ children }) {

    const [user, setUser] = useState()
    const [loading, setLoading] = useState(true)

    const signup = (email, password) => (createUserWithEmailAndPassword(auth, email, password))
    const signin = (email, password) => (signInWithEmailAndPassword(auth, email, password))
    const signout = () => (signOut(auth))

    useEffect(() => {
        onAuthStateChanged(auth, (userCredentials) => {
            setUser(userCredentials)
            setLoading(false)
        })
    }, [])

    return <authContext.Provider
        value={{
            signup,
            signin,
            signout,
            user,
            loading
        }}
    >
        {children}
    </authContext.Provider>
}