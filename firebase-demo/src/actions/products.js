import { collection, getDocs, addDoc } from "firebase/firestore"
import { db } from "../firebase"
import { SET_PRODUCTS } from "../types/products"

const setProducts = products => ({
    type: SET_PRODUCTS,
    payload: products
})

export const getAllProducts = () => dispatch => {
    return new Promise(async (resolve, reject) => {
        const snapshot = await getDocs(collection(db, 'products'))

        let results = []

        snapshot.forEach((doc) => {
            results.push({ id: doc.id, ...doc.data() })
        })

        dispatch(setProducts(results))
    })
}

export const createNewProduct = product => async dispatch => {
    const newProduct = await addDoc(collection(db, 'products'), product)


    console.log('new product created', newProduct)
}

