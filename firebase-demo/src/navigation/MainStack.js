import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from '../pages/Home'
import DashboardHome from '../dashboard/DashboardHome'
import DashboardProducts from '../dashboard/DashboardProducts'
import Login from '../pages/Login'
import Register from '../pages/Register'
import { AuthProvider } from "../context/AuthContext";
import NavBar from "../layout/NavBar";
import ProtectedRoutes from "./ProtectedRoutes";
import Products from "../pages/Products";

export default function MainStack() {

    return <>
        <BrowserRouter>
            <AuthProvider>
                <NavBar>
                    <Routes>
                        <Route
                            path='/'
                            element={
                                <ProtectedRoutes>
                                    <Home />
                                </ProtectedRoutes>
                            }
                        />
                        <Route path='/login' element={<Login />} />
                        <Route path='/register' element={<Register />} />
                        <Route path='/productos' element={<Products />} />
                        <Route
                            path='/dashboard'
                            element={
                                <ProtectedRoutes>
                                    <DashboardHome />
                                </ProtectedRoutes>
                            }
                        />
                        <Route
                            path='/dashboard/productos'
                            element={
                                <ProtectedRoutes>
                                    <DashboardProducts />
                                </ProtectedRoutes>
                            }
                        />
                    </Routes>
                </NavBar>
            </AuthProvider>

        </BrowserRouter>
    </>;
}
