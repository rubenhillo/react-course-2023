import { SET_PRODUCTS } from "../types/products"

const initialState = {

    productDetail: null,
    productList: [{ name: 'Producto 1' }]

}

function productsReducer(state = initialState, action) {

    switch (action.type) {

        case SET_PRODUCTS:
            return { ...state, productList: action.payload }

        default:
            return initialState

    }

}

export default productsReducer