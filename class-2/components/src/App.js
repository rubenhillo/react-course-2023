import RenderingWithProps from './conditional/RenderingWithProps'

import Case1 from './conditional/Case1'
import Case2 from './conditional/Case2'
import Case3 from './conditional/Case3'
import Case4 from './conditional/Case4'
import Map from './lists/Map'

import './index.css'

export default function App() {

    let list = [10, 2, 3, 7, 9, 4, 1, 5]

    return (
        <>
            <div className="container" >
                <div className="flex p-20 flex-col">
                    {
                    /* <RenderingWithProps
                        showContent={true}
                        isBlue={false}
                        isPaid={false}
                        age={17}
                    />

                    <Case1
                        isHello={false}
                    />
                    <Case2 isRequired={false} />
                    <Case3 favorite={false} />
                    <Case4 favorite={true} /> 
                    */}
                    <Map
                        list={list}
                    />

                </div>

            </div >
        </>
    )

}