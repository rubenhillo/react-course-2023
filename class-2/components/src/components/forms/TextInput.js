

export default function TextInput({ label }) {
    return (
        <>
            <label className="uppercase" htmlFor="">{label}</label>
            <input type="text" className="shadow border-radius-lg px-2 py-1 mt-2 ml-2 border " />
        </>
    )

}