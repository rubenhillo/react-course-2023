export default function Case4({ favorite }) {

    return (
        <h1>My favorite fruit is apple {favorite && 'Yes'} </h1>
    )

}