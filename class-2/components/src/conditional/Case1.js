export default function Case1({ isHello }) {

    if (isHello) {
        return <h1>Hello</h1>
    }

    return <h1>Bye</h1>

}